sourceopt.enabled = 1
sourceopt.enable_utf-8_support = 1
sourceopt.removeGenerator = 1
sourceopt.removeBlurScript = 1
sourceopt.removeComments = 1
sourceopt.removeComments.keep.10 = /^TYPO3SEARCH_/usi
sourceopt.headerComment =

# 0 = off
# 1 = no line break at all (code in one line)
# 2 = minimalistic line breaks (structure defining box-elements)
# 3 = aesthetic line breaks (important box-elements)
# 4 = logic line breaks (all box-elements)
sourceopt.formatHtml = 1

sourceopt.formatHtml.tabSize =
sourceopt.formatHtml.debugComment = 0